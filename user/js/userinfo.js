// 1.发送请求,获取用户数据,拿到数据,设置输入框的value值

function renderUser () {
    axios.get('/my/user/userinfo').then(res => {
        // console.log(res);
        // $('input').eq(0).val(res.data.data.username);
        // $('input').eq(1).val(res.data.data.nickname);
        // $('input').eq(2).val(res.data.data.email);

        let form = layui.form; // 使用layui的模块,必须先加载它
        // form.val('表单','数据'); // 这个方法用于数据回填
        // form.val('表单的lay-filter属性值','对象格式的数据,而且要求 对象的键 === input的name值'); // 这个方法用于数据回填
        form.val('abcd',res.data.data);        
    });
}

renderUser();

// 2.表单提交,阻止默认行为,收集数据,ajax提交
$('form').on('submit',function (e) {
    e.preventDefault();
    let data = $(this).serialize(); //serialize不能收集到禁用状态的值;把username这个input禁用
    // console.log(data);
    axios.post('/my/user/userinfo',data).then(res => {
        // console.log(res);
        // 如果更新成功,提示一下,马上让页面左上角更新过来
        if (res.data.status === 0) {
            layer.msg(res.data.message);

            // 注意事项:1.需要iframe把两个页面建立父子关系 2.这个语法只能在真是的服务器环境才能使用(必须使用live)
            // 通过window.parent来调用父页面的函数, 比如让父页面跳转 window.parent.location.href = ''
            window.parent.getUserInfo();
        }
    })
})

// 3.重置
$('button[type=reset]').on('click', function (e) {
    e.preventDefault(); // 阻止默认行为,不要清空表单
    renderUser();
})