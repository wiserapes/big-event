// -------------------- 表单验证 -----------------------
let form = layui.form;
// 1. 密码长度6~12位 (三个input都需要用)
// 2. 新密码和原密码不能一样 (新密码使用)
// 3. 两次新密码必须一致 (重复密码使用)

form.verify ({
    // 1. 密码长度6~12位 (三个input都需要用)
    len: [/^\S{6,12}$/, '长度必须6~12位且不能出现空格'],

    // 2. 新密码和原密码不能一样 (新密码使用)
    diff: function (val) {
        let oldPwd = $('input[name=oldPwd]').val();
        if (val === oldPwd) {
            return '新密码不能和原密码一致';
        }
    },

    // 3. 两次新密码必须一致 (重复密码使用)
    same: function (val) {
        let newPwd = $('input[name=newPwd]').val();
        if (val !== newPwd) {
            return '两次密码不一致';
        }
    }
});

// -------------------- 完成重置密码 -----------------------
$('form').on('submit',function (e) {
    e.preventDefault();
    let data = $(this).serialize();
    axios.post('/my/user/updatepwd', data).then(res => {
        let {status, message} = res.data;
        if (status === 0) {
          layer.msg(message);
          // 这里也有一种业务场景，修改密码后，要退出重新登录
          localStorage.removeItem('token');
          window.parent.location.href = '../login.html';
        }
      });
})
