// 1. ------------------- 查询所有的分类 ---------------------
// 查询的时候.可能没有查到数据
//  没查到数据,从线上演示地址添加几个
function renderCategory() {
    axios.get('/my/category/list').then(res => {
        // console.log(res);
        let {status, data, message} = res.data;
        if (status === 0) {
            let str = '';
            data.forEach(item => {
                str += `
                <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.alias}</td>
                    <td>
                    <button type="button" class="layui-btn layui-btn-xs">编辑</button>
                    <button data-id="${item.id}" type="button" class="layui-btn layui-btn-xs layui-btn-danger">删除</button>
                    </td>
                </tr>`;
            });
            $('tbody').html(str);
        }
    });
}
renderCategory(); // render 渲染  category 类别

// 2. ------------------------ 添加类别 -------------------------
// 2.1 点击 添加类别,出现弹层
let addIndex;
$('button:contains("添加类别")').on('click', function () {
    addIndex = layer.open({
        // 里面的参数去目录中查找
        type: 1,
        title: '添加类别',
        content: $('#tpl-add').html(),
        area: ['500px', '250px']
    });
});
// 2.2 表单提交,阻止默认行为,收集数据,ajax提交
$('body').on('submit', '#add-form', function (e) {
    e.preventDefault();
    let data = $(this).serialize();
    console.log(data);
    axios.post('/my/category/add', data).then(res => {
        if (res.data.status === 0) {
            layer.msg(res.data.message); //提示
            renderCategory(); //重新获取数据,更新
            layer.close(addIndex); // 关闭弹层
        }
    })
})