// ------------------ 切换登录和注册盒子 ------------------
// $('.login a').on('click', function () {
//     $('.login').hide().next().show();
// })

// $('.register a').on('click', function () {
//     $('.login').show().next().hide();
// })

$('.box a').on('click', function () {
    $(this).parents('.box').hide().siblings('.box').show();
});

// ------------------- 注册功能 -----------------------
// 点击按钮,触发表单提交事件,阻止默认行为,收集表单中的数据(用户名,密码),ajax提交到服务器
$('.register form').on('submit', function (e) {
    e.preventDefault();
    // 收集表单数据
    let data = $(this).serialize(); // serialize()是jQuery封装的方法,根据input的name属性获取输入框的值.得到的结果是查询字符串
    // console.log(data);
    // 使用axios发送POST请求
    axios.post('/api/reguser', data).then(res => {
        // console.log(res);
        if(res.data.status === 0){
            // 提示
            layer.msg(res.data.message);
            // 清空输入框
            $('.register form')[0].reset(); // reset()表示重置表单,是一个DOM方法
            // 切换到登录的盒子
            $('.login').show().next().hide();
        }
    })
})

// ------------------- 表单验证 -----------------------
// 加载form模块 (使用layui的内置模块,必须先加载)
let form = layui.form; // let 变量 = layui.模块名;
// console.log(form);
form.verify({
    // 键(规则名称): 值,   // 一个键值对,就是一个验证规则
    // 键(规则名称): 值,   // 值可以是数组   [正则表达式,'验证不通过时的提示']
    // 键(规则名称): 值,   // 值可以是函数

    user: [/\w{2,10}/, '账号长度必须是2~10位!!!'],  // {2,10} 不是 {2, 10}

    pass: [/\S{6,12}/, '密码必须是6~12位, 且不能有空格'], // \S 表示非空白字符

    same: function (val) {
        // val 表示我们输入的确认密码, 哪个input使用了这个规则,形参val表示这个input的值
        // 案例中,确认密码使用了这个验证规则,所以val表示我们输入的确认密码

        let pwd = $('.pwd').val(); // 获取密码
        if (pwd !== val){
            return '两次密码不一致,请重新输入';
        }
    }
});

// ------------------- 登录功能 -----------------------
// 表单提交事件 --> 阻止默认行为 --> 收集数据（账号，密码） --> Ajax提交
$('.login form').on('submit', function (e) {
    e.preventDefault();
    let data = $(this).serialize(); // 接口要求提交 username 和 password，所以要检查 input 的 name
    // console.log(data);
    axios.post('/api/login', data).then(res => {
        console.log(res);
        if (res.data.status === 0) {
            // 把服务器返回的token存储到本地
            localStorage.setItem('token',res.data.token)
            // 提示
            layer.msg(res.data.message, {time: 2000}, function () {
                // 登录成功，跳转到首页 index.html
                location.href = './index.html';  // 这里的路径和js文件在哪里无关。 和两个html文件有关
            });
        }
    })
});