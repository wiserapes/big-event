axios.defaults.baseURL = 'http://www.itcbc.com:8080';


// 发送请求带token
// 1. 单独带。每次发送请求都加（太麻烦）
// 2. 全局配置 axios.defaults.headers['Authorization'] = localStorage.getItem('token');
// 3. 放到请求拦截器里面
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么

    // config 表示请求的配置项，如果我们要加配置项，加到config中即可
    if (config.url.includes('/my/')) {
        config.headers.Authorization = localStorage.getItem('token');
    }

    return config;
},function (error) {
    // 对错误做些什么
    return Promise.reject(error);
})



// 响应拦截器,判断响应结果中的 status 是否为1,如果是1,给出提示
// 添加响应拦截器
axios.interceptors.response.use(
    function (response) {
        // 对响应数据做点什么
        // 如果响应状态码是 小于 400, 执行这个函数
        // console.log(response);
        if (response.data.status === 1){
            layer.msg(response.data.message);
        }
        return response;
    }, function (error) {
        // 对响应错误做点什么
        // 响应状态码,大于等于400,执行这个函数
        // console.log(error.response); // 表示服务器返回的结果
        if (error.response.data.status === 1 && error.response.data.message ===  '身份认证失败'){
            // 说明token有问题
            localStorage.removeItem('token'); //移除假token
            location.href = './login.html';
        }else{
            layer.msg(error.response.data.message);
        }
        
        return Promise.reject(error);
    }
)