// ------------------- 获取用户信息，渲染 -----------------------
function getUserInfo() {
    // 发送请求，获取用户信息
    axios.get('/my/user/userinfo').then(res => {
        // console.log(res);
        if (res.data.status === 0) {
            // 1.设置欢迎您 xxx （优先使用昵称，没有昵称使用账号）
            let name = res.data.data.nickname || res.data.data.username;
            $('.username').text(name);
            // 2.处理头像
            if (res.data.data.user_pic) {
                // 有图片类型的头像
                $('.layui-nav-img').attr('src', res.data.data.user_pic).show();
            }else {
                // 只能截取名字
                let first = res.data.data.username.substring(0,1).toUpperCase();
                $('.text-avatar').text(first).css('display','inline-block');
            }
        }
    });
};
getUserInfo();

// ------------------- 退出功能 -----------------------
$('#logout').on('click', function (e) {
    e.preventDefault();

    layer.confirm('是否这次真的离开我？', function (index) {
        // 点击了确定,执行这个函数
        // 退出,做两件事 (和登录相反的两件事)
        // 1. 清除token
        localStorage.removeItem('token');
        // 2. 跳转到登录页
        location.href = './login.html';

        layer.close(index); // 关闭弹出层
    })
})